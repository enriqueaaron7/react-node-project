
import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import Row from "../new_row/Row";
import "./ListNew.css";

export default class ListNew extends PureComponent {
  static propTypes = {
    newsData: PropTypes.array
  };

  render() {
    const parseDate = (stringDate) => {
      var monthShortNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
        'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
      ];
      const actualDate = new Date();
      const rowDate = new Date(stringDate);

      const difference = actualDate.getTime() - rowDate.getTime(); 
      const differenceOfDays = difference / (1000 * 3600 * 24); 
  
      if (differenceOfDays < 1) {
        const hours = rowDate.getHours() > 12 ? rowDate.getHours() - 12 : rowDate.getHours()
        const lastDatePart = rowDate.getHours() > 12 ? 'pm' : 'am';
        
        return hours.toString() + ':' + rowDate.getMinutes() + ' ' + lastDatePart
      } else if (differenceOfDays >= 1 && differenceOfDays < 2) {
        return 'Yesterday'
      } else {
        return monthShortNames[rowDate.getMonth()] + ' ' + rowDate.getDay()
      }

      console.log(differenceOfDays)
      return stringDate
    }

    return (
      <div className="container">
        {this.props.newsData.map(newInfo => (
          <Row
            key={newInfo._id}
            id={newInfo._id}
            title={newInfo.title}
            createdAt={parseDate(newInfo.createdAt)}
            url={newInfo.url}
            author={newInfo.author ? newInfo.author : ''}
          />
        ))}
      </div>
    );
  }
}