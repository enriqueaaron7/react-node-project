import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import "./Row.css";

export default class Row extends PureComponent {
  static propTypes = {
    id: PropTypes.string,
    title: PropTypes.string,
    author: PropTypes.string,
    url: PropTypes.string,
    createdAt: PropTypes.string,
  };

  constructor(props) {
    super(props);
    this.state = {
      showRow:  true,
    };
    this.hideComponent = this.hideComponent.bind(this);
  }

  hideComponent() {
    this.setState({ showRow: false });
  }

  render() {
    const { showRow } = this.state;
    const apiUrl = 'http://localhost:7000/';

    const openUrl = () => {
      let win = window.open(this.props.url, '_blank');
      win.focus();
    }
    
    const deleteRow = () => {
      fetch(apiUrl + 'news/delete_new?newID=' + this.props.id, {
        method: 'DELETE',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
      }).then(() => this.hideComponent());
    }

    return (
      <>
      {showRow &&
        <div className="new-row" >
          <span className="title" onClick={openUrl} >{this.props.title} - {this.props.author}</span>
          <span className="info">{this.props.createdAt}</span>
          <i className="fa fa-trash icon" onClick={deleteRow} />
        </div>
      }
      </>
    );
  }
}