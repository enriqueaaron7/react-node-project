import React, { PureComponent } from 'react';
import './Header.css';

export default class Header extends PureComponent {
  render() {
    return (
      <div className="container-style">
        <h1 className="title">HN Feed</h1>
        <h3>We &#60;3 hacker news!</h3>
      </div>
    );
  }
}
