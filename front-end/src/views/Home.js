import React, { useEffect, useState } from 'react';
import Header from '../components/header/Header';
import ListNew from '../components/list_news/ListNew';

const url = 'http://localhost:7000/';

const Home = () => {
  const [news, setNews] = useState([]);

  const lastDate = localStorage.getItem('lastDate');
  const cleanData = (arr) => arr.reduce((accumulator, element) => {
    if (element.story_title || element.title) {
      accumulator.push({
        createdAt: element.created_at,
        title: element.story_title ? element.story_title : element.title,
        author: element.author,
        url: element.story_url,
      })
    }

    return accumulator
  }, [])

  const saveData = async (arr) => {
    const response = await fetch(url + 'news/add_news', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(arr)
    });
    await response.json();
    getData();
  }

  const getData = () => {
    fetch(url + 'news/get_news')
      .then(response => response.json())
      .then(data => {
        setNews(data.news)
      })
      .catch(err => console.error(err))
  }

  useEffect(() => {
    fetch('http://hn.algolia.com/api/v1/search_by_date?query=nodejs')
      .then(response => response.json())
      .then(data => {
        console.log(lastDate)
        let finalData = cleanData(data.hits)

        if (lastDate && lastDate !== finalData[0].createdAt) {
          let indexLastItemSaved;
          for (let index = 0; index < finalData.length; index++) {
            if (finalData[index].createdAt === lastDate) {
              indexLastItemSaved = index
              break
            }
          }
          finalData = finalData.slice(0, indexLastItemSaved);
        }

        localStorage.setItem('lastDate', finalData[0].createdAt)
        saveData(finalData)
      })
      .catch(error => console.error(error));
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return(
    <>
      <Header />
      <ListNew  newsData={news}/>
    </>
  )
}

export default Home;
