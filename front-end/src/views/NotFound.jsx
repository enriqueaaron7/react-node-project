import React from 'react';

const NotFound = () => {
  return (
    <>
        <img style={{height: '30%', width: '40%'}} alt="Not found img" src="https://i.imgur.com/qIufhof.png" />
        <div id="info">
            <h3>This page could not be found</h3>
        </div>
    </>
  )
}

export default NotFound;
