import React from 'react';
import { Route, Switch } from "react-router-dom";
import './App.css';

import Home from './views/Home';
import NotFound from './views/NotFound';

const App = () => {
  return (
    <div className="App">
      <Switch>
        <Route path='/' exact component={Home} />
        <Route component={NotFound} />
      </Switch>
    </div>
  );
}

export default App;
