import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { NewsModule } from './news/news.module';
import { MongooseModule } from "@nestjs/mongoose";

@Module({
  imports: [
    NewsModule,
    MongooseModule.forRoot('mongodb://localhost/news-nest-backend', {
      useNewUrlParser: true,
    })],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
