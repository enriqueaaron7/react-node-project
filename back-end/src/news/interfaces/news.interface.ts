import { Document } from 'mongoose';

export interface News extends Document {
  readonly title: string;
  readonly author: string;
  readonly url: string;
  readonly createdAt: Date;
}
