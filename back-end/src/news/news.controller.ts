import {
  Controller,
  Get,
  Post,
  Delete,
  Res,
  HttpStatus,
  Body,
  Query,
} from '@nestjs/common';
import { NewDTO } from './dto/news.dto';
import { NewsService } from './news.service';

@Controller('news')
export class NewsController {

  constructor(private newService: NewsService) {}

  @Post('/add_news')
  addNews(@Res() response, @Body() addNewsDTO: Array<NewDTO>) {
    const success = this.newService.addNews(addNewsDTO);
    console.log(success);
    return success ? response.status(HttpStatus.OK).json({
      message: 'All news added successfully',
    }) : response.status(HttpStatus.INTERNAL_SERVER_ERROR).json({
      message: 'We can not complete te added of the products',
    })
    
  }

  @Get('/get_news')
  async getNews(@Res() response) {
    const news = await this.newService.getNews();
    return response.status(HttpStatus.OK).json({
      news
    });
  }

  // localhost:7000/news/delete_new?newID=
  @Delete('/delete_new')
  async delete_new(@Res() response, @Query('newID') newID) {
    const _new = await this.newService.deleteNews(newID);
    return !_new ? response.status(HttpStatus.BAD_REQUEST).json({
      message: 'Product not found!',
    }) : response.status(HttpStatus.OK).json({
      message: 'Product deleted success',
      _new
    });
  }
}
