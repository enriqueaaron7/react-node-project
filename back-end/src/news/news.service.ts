import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';

import { News } from './interfaces/news.interface';
import { NewDTO } from './dto/news.dto';

@Injectable()
export class NewsService {

  constructor(@InjectModel('News') private newsModel: Model<News>) {}

  async getNews(): Promise<News[]> {
    const news = await this.newsModel.find();
    return news;
  }

  addNews(productDTO: Array<NewDTO>): boolean {
    productDTO.forEach(async (element) => {
      const product = await this.newsModel.create(element);
      await product.save()
    })

    return true;
  }

  async deleteNews(productID: string): Promise<News> {
    return await this.newsModel.findByIdAndDelete(productID);
  }
}
