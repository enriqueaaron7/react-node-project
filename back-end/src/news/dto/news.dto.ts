export class AddNewsDTO {
  data: NewDTO[];
}

export class NewDTO {
  readonly title: string;
  readonly author: string;
  readonly url: string;
  readonly createdAt: Date;
}
