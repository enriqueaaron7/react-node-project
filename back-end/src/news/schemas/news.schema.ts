import { Schema } from 'mongoose';

export const NewsSchema = new Schema({
  title: {
    type: String,
    required: true
  },
  author: String,
  url: {
    type: String,
    required: true
  },
  createdAt: {
    type:Date,
    default: Date.now,
  },
})
