# Simple notice page
This is a web page to review the latest notice about nodejs

## About the app
The app have a front-end created with ReactJs and a back-end created with NestJs. Also we use a MongoDB database

## Pre-Requisites
node:     14.15.0v (2020 LTS version)
yarn:     1.22.5v

## Run the project (manually)
You can move into each folder to run the front and the back of the web page

For each one you just need to run the command `yarn start` and you will have the project running into the port `3000` (front-end) and `7000` (backend)

## Run the project (Docker)
TODO

## Author
Aaron Carrasco M.
